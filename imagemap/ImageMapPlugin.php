<?php namespace Craft;

class ImageMapPlugin extends BasePlugin
{

    protected   $_version = '0.1.0',
                $_developer = 'Mats Mikkel Rummelhoff',
                $_developerUrl = 'http://mmikkel.no',
                $_pluginName = 'Image Map',
                $_pluginUrl = 'https://github.com/mmikkel',
                $_minVersion = '2.5';

    public function getName()
    {
        return $this->_pluginName;
    }

    public function getVersion()
    {
        return $this->_version;
    }

    public function getDeveloper()
    {
        return $this->_developer;
    }

    public function getDeveloperUrl()
    {
        return $this->_developerUrl;
    }

    public function getPluginUrl()
    {
        return $this->_pluginUrl;
    }

    public function getSettingsHtml()
    {
        $fieldLayout = craft()->fields->getLayoutByType('ImageMap_Point');
        return craft()->templates->render('imagemap/_settings', array(
            'settings' => $this->getSettings(),
            'fieldLayout' => $fieldLayout
        ));
    }

    public function prepSettings($settings)
    {
        // Delete the old field layout
        craft()->fields->deleteLayoutsByType('ImageMap_Point');

        // Extract the field layout data from the settings
        $postedFieldLayout = isset($settings['fieldLayout']) ? $settings['fieldLayout'] : array();
        $requiredFields = isset($settings['requiredFields']) ? $settings['requiredFields'] : array();

        // Save the field layout
        $fieldLayout = craft()->fields->assembleLayout($postedFieldLayout, $requiredFields, true);
        $fieldLayout->type = 'ImageMap_Point';
        craft()->fields->saveLayout($fieldLayout);

        // Return the settings without the field layout data
        unset($settings['fieldLayout'], $settings['requiredFields']);
        return $settings;
    }

    public function defineSettings()
    {
        return array(
            'fieldLayout' => AttributeType::Mixed,
            'requiredFields' => AttributeType::Mixed,
        );
    }

    // TODO: On uninstall, delete all ImageMap_Point elements (craft()->elements->deleteElementsByType('ImageMap_Point'))

}