<?php
namespace Craft;

class ImageMap_PointElementType extends BaseElementType
{

    // Public Methods
    // =========================================================================
    /**
     * @inheritDoc IComponentType::getName()
     *
     * @return string
     */
    public function getName()
    {
        return Craft::t('Image Map') . ' ' . Craft::t('Point');
    }
    /**
     * @inheritDoc IElementType::hasContent()
     *
     * @return bool
     */
    public function hasContent()
    {
        return true;
    }
    /**
     * @inheritDoc IElementType::hasTitles()
     *
     * @return bool
     */
    public function hasTitles()
    {
        return true;
    }
    /**
     * @inheritDoc IElementType::isLocalized()
     *
     * @return bool
     */
    public function isLocalized()
    {
        return true;
    }

    /**
     * @inheritDoc IElementType::hasStatuses()
     *
     * @return bool
     */
    public function hasStatuses()
    {
        return false;
    }

    /**
     * @inheritDoc IElementType::defineCriteriaAttributes()
     *
     * @return array
     */
    // public function defineCriteriaAttributes()
    // {
    //     return array(
    //         'after'           => AttributeType::Mixed,
    //         'authorGroup'     => AttributeType::String,
    //         'authorGroupId'   => AttributeType::Number,
    //         'authorId'        => AttributeType::Number,
    //         'before'          => AttributeType::Mixed,
    //         'editable'        => AttributeType::Bool,
    //         'expiryDate'      => AttributeType::Mixed,
    //         'order'           => array(AttributeType::String, 'default' => 'lft, postDate desc'),
    //         'postDate'        => AttributeType::Mixed,
    //     );
    // }

    /**
     * @inheritDoc IElementType::modifyElementsQuery()
     *
     * @param DbCommand            $query
     * @param ElementCriteriaModel $criteria
     *
     * @return bool|false|null|void
     */
    // public function modifyElementsQuery(DbCommand $query, ElementCriteriaModel $criteria)
    // {
    //     return null;
    // }

    /**
     * @inheritDoc IElementType::populateElementModel()
     *
     * @param array $row
     *
     * @return BaseElementModel|BaseModel|void
     */
    public function populateElementModel($row)
    {
        return ImageMap_PointModel::populateModel($row);
    }

    /**
     * @inheritDoc IElementType::getTableAttributeHtml()
     *
     * @param BaseElementModel $element
     * @param string           $attribute
     *
     * @return mixed|null|string
     */
    public function getTableAttributeHtml(BaseElementModel $element, $attribute)
    {
        return null;
    }


    /**
     * @inheritDoc IElementType::getEditorHtml()
     *
     * @param BaseElementModel $element
     *
     * @return string
     */
    public function getEditorHtml(BaseElementModel $element)
    {

        $html = '';

        // Title
        $html = craft()->templates->renderMacro('_includes/forms', 'textField', array(
            array(
                'label'     => Craft::t('Title'),
                'locale'    => $element->locale,
                'id'        => 'title',
                'name'      => 'title',
                'value'     => $element->getContent()->title,
                'errors'    => $element->getErrors('title'),
                'first'     => true,
                'autofocus' => true,
                'required'  => true
            )
        ));

        // Field layout
        $fieldLayout = $element->getFieldLayout();

        if ($fieldLayout)
        {
            $originalNamespace = craft()->templates->getNamespace();
            $namespace = craft()->templates->namespaceInputName('fields', $originalNamespace);
            craft()->templates->setNamespace($namespace);
            foreach ($fieldLayout->getFields() as $fieldLayoutField)
            {
                $fieldHtml = craft()->templates->render('_includes/field', array(
                    'element'  => $element,
                    'field'    => $fieldLayoutField->getField(),
                    'required' => $fieldLayoutField->required
                ));
                $html .= craft()->templates->namespaceInputs($fieldHtml, 'fields');
            }
            craft()->templates->setNamespace($originalNamespace);
        }

        return $html;

    }

    /**
     * @inheritdoc BaseElementType::saveElement()
     *
     * @return bool
     */
    public function saveElement(BaseElementModel $element, $params)
    {
        return craft()->imageMap->savePoint($element);
    }

}