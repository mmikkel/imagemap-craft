<?php
namespace Craft;

class ImageMapFieldType extends BaseFieldType
{

	public function getName()
    {
        return Craft::t('Image Map');
    }

	// Properties
	// =========================================================================

	/**
	 * Template to use for field rendering.
	 *
	 * @var string
	 */
	protected $inputTemplate = 'imagemap/_fieldtype/input';

	// TODO: onAfterSave, create relation between point elements and entry (and also field, I guess?)
	// onEntryDelete – delete all related point elements
	// Task: Delete orphaned point elements

	// Public Methods
	// =========================================================================
	/**
	 * @inheritDoc ISavableComponentType::getSettingsHtml()
	 *
	 * @return string|null
	 */
	public function getSettingsHtml()
	{
		return craft()->templates->render('imagemap/_fieldtype/settings', array(
			'fieldLayout' => $this->getFieldLayout(),
		));
	}

	public function getFieldLayout()
	{
		$settings = $this->getSettings();
		$fieldLayout = isset($settings['fieldLayout']) && $settings['fieldLayout'] ? $settings['fieldLayout'] : array();
		$requiredFields = isset($settings['requiredFields']) && $settings['requiredFields'] ? $settings['requiredFields'] : array();
		return craft()->fields->assembleLayout($fieldLayout, $requiredFields);
	}

	// public function prepSettings($settings)
	// {
	// 	// Extract the field layout data from the settings
 //    	$postedFieldLayout = isset($settings['fieldLayout']) ? $settings['fieldLayout'] : array();
 //    	$requiredFields = isset($settings['requiredFields']) ? $settings['requiredFields'] : array();
	// }

	public function onAfterElementSave()
	{
		$fieldHandle = $this->model->handle;
		$element = $this->element;
		$value = $element->$fieldHandle;
		// Save relations for point elements
        $pointElementIds = array();
		foreach ($value['points'] as $point)
		{
			if (isset($point['elementId']) && $point['elementId'])
			{
				$pointElementIds[] = $point['elementId'];
			}
		}
		craft()->relations->saveRelations($this->model, $element, $pointElementIds);
		// Delete elements that have no relation

	}

	// public function onAfterSave()
	// {
	// 	ImageMapPlugin::log('onAfterSave!');
	// }

	public function onBeforeDelete()
	{
		$criteria = craft()->elements->getCriteria('ImageMap_Point');
		$criteria->relatedTo(array(
			'field' => $this->model,
		));
	}

	// Protected Methods
	// =========================================================================
	protected function defineSettings()
    {
        return array(
            'fieldLayout' => AttributeType::Mixed,
        	'requiredFields' => AttributeType::Mixed,
        );
    }

	/**
	 * @inheritDoc BaseElementFieldType::getInputSelectionCriteria()
	 *
	 * @return array
	 */
	protected function getInputSelectionCriteria()
	{
		$settings = $this->getSettings();
		$allowedKinds = array();
		if (isset($settings->restrictFiles) && !empty($settings->restrictFiles) && !empty($settings->allowedKinds))
		{
			$allowedKinds = $settings->allowedKinds;
		}
		return array('kind' => $allowedKinds);
	}

	/**
	 * @inheritDoc BaseElementFieldType::getInputSources()
	 *
	 * @throws Exception
	 * @return array
	 */
	protected function getInputSources()
	{
		// Look for the single folder setting
		$settings = $this->getSettings();
		if ($settings->useSingleFolder)
		{
			$folderId = $this->_determineUploadFolderId($settings);
			craft()->userSession->authorize('uploadToAssetSource:'.$folderId);
			$folderPath = 'folder:'.$folderId.':single';
			return array($folderPath);
		}
		$sources = array();
		// If it's a list of source IDs, we need to convert them to their folder counterparts
		if (is_array($settings->sources))
		{
			foreach ($settings->sources as $source)
			{
				if (strncmp($source, 'folder:', 7) === 0)
				{
					$sources[] = $source;
				}
			}
		}
		else if ($settings->sources == '*')
		{
			$sources = '*';
		}
		return $sources;
	}

	/**
	 * Returns the locale that target elements should have.
	 *
	 * @return string
	 */
	protected function getTargetLocale()
	{
		if (craft()->isLocalized())
		{
			$targetLocale = isset($this->getSettings()->targetLocale) ? $this->getSettings()->targetLocale : null;
			if ($targetLocale)
			{
				return $targetLocale;
			}
			else if (isset($this->element))
			{
				return $this->element->locale;
			}
		}
		return craft()->getLanguage();
	}

	/**
	 * Returns an array of variables that should be passed to the input template.
	 *
	 * @param string $name
	 * @param mixed  $value
	 *
	 * @return array
	 */
	protected function getInputTemplateVariables($name, $value)
	{

		$settings = $this->getSettings();

		// Get criteria for selecting elements
		$selectionCriteria = $this->getInputSelectionCriteria();
		$selectionCriteria['localeEnabled'] = null;
		$selectionCriteria['locale'] = $this->getTargetLocale();

		return array(
			'name' => $name,
			'points' => isset($value['points']) ? $value['points'] : null,
			'fieldId'            => $this->model->id,
			'elementSelectConfig' => array(
				'jsClass'            => 'Craft.AssetSelectInput',
				'elementType'        => new ElementTypeVariable(craft()->elements->getElementType(ElementType::Asset)),
				'id'                 => craft()->templates->formatInputId('assetSelect-'.$name),
				'fieldId'            => $this->model->id,
				'storageKey'         => 'field.'.$this->model->id,
				'name'               => $name.'[assetId]',
				'elements'           => isset($value['asset']) ? $value['asset'] : null,
				'sources'            => '*',///$this->getInputSources(),
				'criteria'           => $selectionCriteria,
				'sourceElementId'    => (isset($this->element->id) ? $this->element->id : null),
				'limit'              => 1,//($this->allowLimit ? $settings->limit : null),
				'selectionLabel'     => Craft::t('Choosy choose'),//Craft::t($this->getSettings()->selectionLabel),
			),
		);
	}

	/**
	 * @inheritDoc IFieldType::getInputHtml()
	 *
	 * @param string $name
	 * @param mixed  $criteria
	 *
	 * @return string
	 */
	public function getInputHtml($name, $value)
	{

		// Reformat the input name into something that looks more like an ID
	    $id = craft()->templates->formatInputId($name);

	    // Figure out what that ID is going to look like once it has been namespaced
	    $namespacedId = craft()->templates->namespaceInputId($id);

    	// Get input template variables
    	$variables = $this->getInputTemplateVariables($name, $value);

    	// Include resources
    	craft()->templates->includeCssResource('imagemap/fieldtype/input.css');
    	craft()->templates->includeJsResource('imagemap/fieldtype/input.js');
    	craft()->templates->includeJs("$('#{$namespacedId}-field').craftImageMap();");

    	return craft()->templates->render($this->inputTemplate, $variables);

	}

	public function prepValue($value)
	{
		$value = (is_array($value) && !empty($value) ? $value : array(
			'asset' => null,
			'points' => array(),
		));
		// Get the selected asset
		if (isset($value['assetId']) && $value['assetId'])
		{
			// Get selected element criteria
			$criteria = craft()->elements->getCriteria(ElementType::Asset);
			$criteria->id = $value['assetId'];
			$criteria->status = null;
			$criteria->localeEnabled = null;
			$value['asset'] = $criteria;
			unset($value['assetId']);
		}
		$value['points'] = is_string($value['points']) ? (array) json_decode($value['points'], JSON_PRETTY_PRINT) : $value['points'];
		// For front end requests, replace point elementIds with ElementCriteriaModel
		if (!craft()->request->isCpRequest() && is_array($value['points']) && !empty($value['points']))
		{
			for ($i = 0; $i < count($value['points']); ++$i)
			{
				$point = $value['points'][$i];
				$point['element'] = isset($point['elementId']) && $point['elementId'] ? craft()->elements->getCriteria('ImageMap_Point', array(
					'id' => $point['elementId'],
					'limit' => 1,
				))->first() : null;
				unset($point['elementId']);
				$value['points'][$i] = $point;
			}
		}
		return $value;
	}

	public function prepValueFromPost($value)
    {
    	$value['points'] = isset($value['points']) ? (array) json_decode($value['points'], JSON_PRETTY_PRINT) : array();
    	return $value;
    }

    public function defineContentAttribute()
    {
        return array(AttributeType::Mixed, 'column' => ColumnType::Text);
    }

}