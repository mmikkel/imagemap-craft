<?php
namespace Craft;

class ImageMap_PointModel extends BaseElementModel
{

    // Properties
    // =========================================================================
    /**
     * @var string
     */
    protected $elementType = 'ImageMap_Point';

    // Public Methods
    // =========================================================================

    /**
     * @inheritDoc BaseElementModel::isEditable()
     *
     * @return bool
     */
    public function isEditable()
    {
        // TODO: Think about permissions...
        return true;
        // return (
        //     craft()->userSession->checkPermission('publishEntries:'.$this->sectionId) && (
        //         $this->authorId == craft()->userSession->getUser()->id ||
        //         craft()->userSession->checkPermission('publishPeerEntries:'.$this->sectionId) ||
        //         $this->getSection()->type == SectionType::Single
        //     )
        // );
    }

    public function getFieldLayout()
    {
        if ($field = $this->_getField())
        {
            // Get fieldLayout from ImageMap field instance
            $fieldSettings = $field->fieldType->getSettings();
            $fieldLayout = isset($fieldSettings['fieldLayout']) && $fieldSettings['fieldLayout'] ? $fieldSettings['fieldLayout'] : array();
            $requiredFields = isset($fieldSettings['requiredFields']) && $fieldSettings['requiredFields'] ? $fieldSettings['requiredFields'] : array();
            return craft()->fields->assembleLayout($fieldLayout, $requiredFields);
        }
        else
        {
            // Get field layout stored from plugin
            return craft()->fields->getLayoutByType('ImageMap_Point');
        }
        return null;
    }

    // Protected Methods
    // =========================================================================
    /**
     * @inheritDoc BaseModel::defineAttributes()
     *
     * @return array
     */
    protected function defineAttributes()
    {
        return array_merge(parent::defineAttributes(), array(
            'fieldId'     => AttributeType::Number,
        ));
    }

    // Private Methods
    // =========================================================================
    private function _getField()
    {
        return craft()->fields->getFieldById($this->fieldId);
    }

    //======================================================================
    // Define Behaviors
    //======================================================================
    public function behaviors()
    {
        return array(
            'fieldLayout' => new FieldLayoutBehavior('ImageMap_Point'),
        );
    }

}