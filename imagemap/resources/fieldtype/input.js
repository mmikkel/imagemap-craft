$.fn.craftImageMap = function () {

	var ImageMapField = {

		init : function (container)
		{

			this.$field = $(container);
			this.$pointInput = this.$field.find('input.imagemap-canvas__points-input');
			this.$frame = this.$field.find('.imagemap-canvas__frame');
			this.$canvas = this.$field.find('.imagemap-canvas');

			this.fieldId = this.$canvas.data('field');
			this.entryId = $('#main input[type="hidden"][name="entryId"]').val() || null;
			this.locale = $('#main input[type="hidden"][name="locale"]').val() || null;

			if (this.$canvas.length === 0 || this.$pointInput.length === 0) return this;

			// Create current points
			var points = this.$pointInput.val() ? $.parseJSON(this.$pointInput.val()) : [];
			console.log(points);
			for (var i = 0; i < points.length; ++i)
			{
				this.createPoint(points[i]);
			}

			// Add canvas event listeners
			this.$canvas
				.on('click', $.proxy(this.onCanvasClick, this))
				.on('mousedown touchstart', '.imagemap-canvas__point', $.proxy(this.onPointMouseDown, this))
				.on('dblclick', '.imagemap-canvas__point', $.proxy(this.onPointDoubleClick, this))
				.on('dragstart', 'img', function (e) { return false; });

			// Init element select field
			$(document).ready($.proxy(function () {

				var $elementSelectContainer = this.$field.find('.imagemap-element-select'),
					$elementSelect = $elementSelectContainer.length > 0 ? $elementSelectContainer.find('.elementselect:first') : false;

				// Offset this to RAF, if not the element select property will be null when switching between entry types :/
				Garnish.requestAnimationFrame($.proxy(function () {
					var elementSelect = $elementSelect && $elementSelect.length > 0 ? $elementSelect.data('elementSelect') : false;
					if (elementSelect)
					{
						elementSelect.on('selectElements', $.proxy(function(response) {
							if (response && response.elements && response.elements.length > 0)
							{
								this.updateFrame(response.elements[0]);
							}
							else
							{
								this.updateFrame(false);
							}
						}, this));
					}
				}, this));

			}, this));

			// TODO – Look for Craft/Garnish resize event
			$(window).resize($.proxy(this.onWindowResize, this));

		},

		createPoint : function (attributes)
		{

			// Create point and add position to input
			var	$point = $('<div class="imagemap-canvas__point"><span class="icon" /></div>');

			this.updatePoint($point, attributes);

			$point.appendTo(this.$canvas);

			var $pointIcon = $point.children('.icon:first'),
				$pointMenu = $('<div class="menu imagemap-canvas-menu" data-align="center"/>').insertAfter($pointIcon),
				$pointMenuUl = $('<ul/>').appendTo($pointMenu);

			$('<li><a data-action="edit">' + Craft.t('Edit point') + '</a></li>').appendTo($pointMenuUl);
			$('<li><a data-action="remove">' + Craft.t('Remove point') + '</a></li>').appendTo($pointMenuUl);

			$pointMenu.find('a').each(function (){
				$(this).data('point', $point);
			});

			var pointMenu = new Garnish.MenuBtn($pointIcon, {
				onOptionSelect : $.proxy(this.onPointMenuOptionSelect, this)
			});

			$point.data('menu', pointMenu);

			return $point;

		},

		updatePoint : function ($point, attributes)
		{
			$point.css({
				left : attributes.coordinates.xPerc + '%',
				top : attributes.coordinates.yPerc + '%'
			}).data(attributes);
			if (attributes.hasOwnProperty('elementId') && attributes.elementId) $point.addClass('has-element');
		},

		refresh : function () {
			// Get points and update input
			var $points = this.$canvas.find('.imagemap-canvas__point'),
				points = $.map($points, function (point) {
					var $point = $(point),
						data = $point.data();
					if (data.hasOwnProperty('elementId') && data.elementId > 0) $point.addClass('has-element');
					else $point.removeClass('has-element');
					return {
						coordinates : data.coordinates || {
							x : 0,
							y : 0,
							xPerc : 0,
							yPerc : 0
						},
						elementId : data.elementId || null
					}
				});
			this.$pointInput.attr('value', JSON.stringify(points));
		},

		updateFrame : function (asset)
		{
			if (asset && asset.url)
			{
				var fileExtension = asset.url.split('.').pop(),
					fileKind = ['jpg', 'gif', 'jpeg', 'png', 'webp', 'svg'].indexOf(fileExtension.toLowerCase()) > -1 ? 'image' : 'video';
				this.$frame.html(this.templates[fileKind + 'FrameContent'](asset.url));
				this.$canvas.addClass('has-asset');
			}
			else
			{
				this.$canvas.removeClass('has-asset');
				this.$frame.empty();
			}
			var numPoints = this.$canvas.find('.imagemap-canvas__point').length;
			if (numPoints > 0)
			{
				var clearPoints = window.confirm(Craft.t('Your image map Asset has changed.') + ' ' + Craft.t('Clear existing points?'));
				if (clearPoints)
				{
					this.clearPoints();
				}
			}
		},

		clearPoints : function ()
		{
			this.$canvas.find('.imagemap-canvas__point').remove();
		},

		createElementModal : function(settings)
		{
			return new Craft.BaseElementSelectorModal('ImageMap_Point', $.extend({
				limit: null,
				multiSelect : true,
				closeOtherModals: true,
				criteria : null,
				sources : '*'
			}, settings));
		},

		getMouseEventCoordinates : function (e)
		{
			var pointX = e.pageX - this.$canvas.offset().left,
				pointY = e.pageY - this.$canvas.offset().top;
			return {
				x : pointX,
				y : pointY,
				xPerc : (pointX / this.$canvas.width()) * 100,
				yPerc : (pointY / this.$canvas.height()) * 100
			}
		},

		onPointMenuOptionSelect : function(option)
		{

			var $option = $(option),
				action = $option.data('action'),
				$element = $option.data('point'),
				elementId = $element.data('elementId') || null;

			switch (action) {

				case 'edit' :

					var elementEditor = new Craft.ElementEditor({
						hudTrigger : $element,
						elementType : 'ImageMap_Point',
						elementId : elementId,
						locale : this.locale,
						attributes: {
							ownerId : this.entryId,
							fieldId : this.fieldId
						},
						// onBeginLoading : $.proxy(function()
						// {
						// 	//this.$newEntryBtn.addClass('loading');
						// }, this),
						// onEndLoading : $.proxy(function()
						// {
						// 	//this.$newEntryBtn.removeClass('loading');
						// }, this),
						// onHideHud : $.proxy(function()
						// {
						// 	//this.$newEntryBtn.removeClass('inactive').text(newEntryBtnText);
						// }, this),
						onSaveElement : $.proxy(function(response)
						{
							if (response && response.hasOwnProperty('id') && response.id)
							{
								$element.data('elementId', response.id);
								this.refresh();
							}
							// // Make sure the right section is selected
							// var sectionSourceKey = 'section:'+sectionId;

							// if (this.sourceKey != sectionSourceKey)
							// {
							// 	this.selectSourceByKey(sectionSourceKey);
							// }

							// this.selectElementAfterUpdate(response.id);
							// this.updateElements();
						}, this)
					});

					break;

				case 'remove' :
						if (elementId)
						{
							var actionUrl = Craft.getActionUrl('imagemap/points/delete');
							$.ajax(actionUrl, {
								elementId : elementId,
								success : function (response)
								{
									console.log(response);
								},
								error : function ()
								{
									alert(Craft.t('Something went wrong!'));
								}
							});
						}
						$element.remove();
						this.refresh();
						break;

			}

		},

		onCanvasClick : function (e)
		{

			// Hide all point menus
			if (this.openPointMenu)
			{
				this.openPointMenu.hideMenu();
				this.openPointMenu = null;
			}

			if (this.$draggingPoint)
			{
				this.$canvas.trigger('mouseleave');
				return false;
			}

			if ($(e.target).hasClass('imagemap-canvas__point')) return true;

			e.preventDefault();
			e.stopPropagation();

			// Create point
			$point = this.createPoint({
				coordinates : this.getMouseEventCoordinates(e),
				elementId : null
			});

			this.refresh();

		},

		onPointMouseDown : function (e)
		{
			if (this.$draggingPoint) return false;

			this.$draggingPoint = $(e.currentTarget);

			e.preventDefault();
			e.stopPropagation();

			this.$canvas
				.on('mousemove touchmove', $.proxy(this.onCanvasMouseMove, this))
				.on('mouseleave', $.proxy(this.onCanvasMouseLeave, this));

			return false;

		},

		onCanvasMouseLeave : function (e)
		{
			this.$draggingPoint = null;
			this.$canvas
				.off('mousemove touchmove', $.proxy(this.onCanvasMouseMove, this))
				.off('mouseleave', $.proxy(this.onCanvasMouseLeave, this));
			this.refresh();
		},

		onCanvasMouseMove : function (e)
		{
			if (!this.$draggingPoint) return false;

			var $point = this.$draggingPoint;

			this.updatePoint($point, $.extend($point.data(), {
				coordinates : this.getMouseEventCoordinates(e)
			}));
		},

		onPointDoubleClick : function (e)
		{
			e.preventDefault();
			e.stopPropagation();
			var $point = $(e.currentTarget);
			this.openPointMenu = $(e.currentTarget).data('menu');
			this.openPointMenu.showMenu();
			this.$draggingPoint = null;
		},

		onWindowResize : function (e)
		{
			if (this.openPointMenu)
			{
				this.openPointMenu.hideMenu();
				this.openPointMenu = null;
			}
		},

		templates : {

			imageFrameContent : function (url)
			{
				return '<img src="' + url + '" alt="" />';
			},

			videoFrameContent : function (url)
			{
				return '<p>video not supported yet..</p>';
			}

		}

	};

	ImageMapField.init(this);

	return this;

}