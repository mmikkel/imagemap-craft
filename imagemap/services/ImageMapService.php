<?php
namespace Craft;

class ImageMapService extends BaseApplicationComponent
{

    /**
     * Saves a new or existing point
     * @param ImageMap_PointModel $point The point to be saved.
     *
     * @throws \Exception
     * @return bool Whether the point was saved successfully.
     */
    public function savePoint(ImageMap_PointModel $point)
    {
        return craft()->elements->saveElement($point);
    }

    public function deletePointsByIds($pointIds)
    {
        if (!is_array($pointIds)) {
            $pointIds = array($pointIds);
        }
        foreach ($pointIds as $pointId) {
            craft()->elements->deleteElementById($pointId);
        }
    }

}